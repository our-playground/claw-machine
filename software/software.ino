#include <Servo.h>

/*create servo object to control a servo*/
Servo myservo;

/*INPUT PINS*/
const int coinslotpin = 2;//interrupt
const int X1 = 3;//LEFT
const int Y1 = 4;//DOWN
const int X2 = 5;//RIGHT
const int Y2 = 6;//UP
const int OK = 7;//TRIGGER

/*OUTPUT PINS*/
const int latchpin = 8;
const int clockpin = 9;
const int datapin = 10;

/*input status variable declaration*/
volatile int coinslot,NX,NY,PX,PY,TRIGGER;

/*counter variable declaration*/
volatile int token = 0;
unsigned long timer;
long int timeend;

/*shift register byte*/
byte bytesignal = 0;
int to_convert_into_byte = 0;

/*default servo grip level - changeable*/
int open_level = 0;
int close_level = 180;

void setup(){
  /*input control pins initialization*/
  pinMode(coinslotpin,INPUT);
  pinMode(X1,INPUT);
  pinMode(Y1,INPUT);
  pinMode(X2,INPUT);
  pinMode(Y2,INPUT);
  myservo.attach(OK);

  /*shift register pins initialization*/
  pinMode(latchpin,OUTPUT);
  pinMode(clockpin,OUTPUT);
  pinMode(datapin,OUTPUT);
  
  /*set attach interrupt to pin 2*/
  attachInterrupt(digitalPinToInterrupt(coinslotpin), coin_slot, HIGH);
}

void loop(){
  timer = millis();
  while(not token == 0){
    /*startup*/
    delay(3000);
    
    /*30 seconds to set 30 secs timer*/
    timeend = timer + 30;

    /*control block*/
    while(timer <= timeend){
      /*print countdown time if changed*/
      //print here using shift register
      
      /*listen to all input controls*/
      xyz_control();
      
      if(TRIGGER == HIGH){
        /*catching up sequence*/
        catchmeup();
        break;
      }else{
        /*control updater for X*/
        if(NX == HIGH){
          to_convert_into_byte += 3;
        }else if(PX == HIGH){
          to_convert_into_byte += 2;
        }
        
        /*control updater for Y*/
        if(NY == HIGH){
          to_convert_into_byte += 1;
        }else if(PY == HIGH){
          to_convert_into_byte += 0;
        }
        
        /*register update for X and Y controls*/
        bitSet(bytesignal,to_convert_into_byte);
        updateShiftRegister();
      }
    }
    token--;
  }
  delay(1000);
}

void xyz_control(){
  NX = digitalRead(X1);
  NY = digitalRead(Y1);
  PX = digitalRead(X2);
  PY = digitalRead(Y2);
  TRIGGER = digitalRead(OK);
}
void catchmeup(){
  /*claw down*/
  //add while loop that'll take long for some seconds
  to_convert_into_byte += 4;
  bitSet(bytesignal,to_convert_into_byte);
  updateShiftRegister();
  
  /*close claw*/
  //control servo
  myservo.write(closelvl);
  
  /*claw up*/
  //add while loop that'll take long for some seconds
  to_convert_into_byte += 5;
  bitSet(bytesignal,to_convert_into_byte);
  updateShiftRegister();
  
  /*digital write both x and y axis to origin*/
  //add while loop that'll take long for some seconds
  to_convert_into_byte += 3;
  bitSet(bytesignal,to_convert_into_byte);
  updateShiftRegister();
  //add while loop that'll take long for some seconds
  to_convert_into_byte += 1;
  bitSet(bytesignal,to_convert_into_byte);
  updateShiftRegister();

  /*open claw*/
  //control servo
  myservo.write(open_lvl);
}
void coin_slot(){
  coinslot = digitalRead(coinslotpin);
  if(coinslot == HIGH){
    token += 1;
  }
}

void updateShiftRegister(){
  digitalWrite(latchpin, LOW);
  shiftOut(datapin, clockpin, LSBFIRST, bytesignal);
  digitalWrite(latchpin, HIGH);
  bytesignal = 0;
  to_convert_into_byte = 0;
}
